<?php

class ControladorDetallesVenta{

    /*=============================================
	Seleccionar Registros
	=============================================*/

	static public function ctrSeleccionarDetallesVenta($item, $valor){

		$tabla = "detalle";

		$respuesta = ModeloDetallesVenta::mdlSeleccionarDetallesVenta($tabla, $item, $valor);

		return $respuesta;

	}

	
	/*=============================================
	Registro
	=============================================*/

	static public function ctrRegistroDetallesVenta(){

		if(isset($_POST["idVenta"])){

			$tabla = "detalle";

			$datos = array("idVenta" => $_POST["idVenta"],
						   "idProducto" => $_POST["idProducto"],
						   "cantidad" => $_POST["cantidad"]);

			$respuesta = ModeloDetallesVenta::mdlRegistroDetallesVenta($tabla, $datos);

			if($respuesta == "ok"){

				echo '<script>

					if ( window.history.replaceState ) {

						window.history.replaceState( null, null, window.location.href );

					}

					window.location = "index.php?pagina=ventas";

				</script>';

			}else{
				echo $respuesta;
			}
		}
	}

}

// /*=============================================
// 	Eliminar Registro
// 	=============================================*/
// 	public function ctrEliminarUsuario(){

// 		if(isset($_GET["idEliminarUsuario"])){

// 			$tabla = "usuario";
// 			$valor = $_GET["idEliminarUsuario"];

// 			$respuesta = ModeloUsuarios::mdlEliminarUsuarios($tabla, $valor);

// 			if($respuesta == "ok"){

// 				echo '<script>

// 				Swal.fire({
					
// 					icon: "success",
// 					title: "El usuario se eliminó correctamente",
// 					showConfirmButton: true,
// 					confirmButtonText:"Cerrar"
// 				  }).then(function(result){
// 					if(result.value){
// 						window.location = "index.php?pagina=usuarios";
// 					}
// 				  });

// 				</script>';

// 			}

// 		}

// 	}
// }