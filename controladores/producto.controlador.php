<?php

class ControladorProducto{

    /*=============================================
	Seleccionar Registros
	=============================================*/

	static public function ctrSeleccionarProductos($item, $valor){

		$tabla = "producto";

		$respuesta = ModeloProductos::mdlSeleccionarProductos($tabla, $item, $valor);

		return $respuesta;

	}

	
	/*=============================================
	Registro
	=============================================*/
	static public function ctrRegistroProducto(){

		if(isset($_POST["nombre"])){

			if(preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nombre"])){
				//validar no existe email

				$item = "nombre";
				$valor = $_POST["nombre"];

				$producto = ControladorProducto::ctrSeleccionarProductos($item, $valor);
			
				if(!$producto){

					$tabla = "producto";

					$datos = array("nombre" => $_POST["nombre"],
									"marca" => $_POST["marca"],
									"detalle" => $_POST["detalle"],
									"precio" => $_POST["precio"],
									"stock" => $_POST["stock"],
									"estado" => 1);

								

					$respuesta = ModeloProductos::mdlRegistroProducto($tabla, $datos);
				
					if($respuesta == "ok"){

						echo '<script>

							if ( window.history.replaceState ) {

								window.history.replaceState( null, null, window.location.href );

							}

							window.location = "index.php?pagina=productos";

						</script>';
					}
					
				}else{

					echo '<script>

					Swal.fire({
						
						icon: "error",
						title: "El producto ya existe en la BD",
						showConfirmButton: true,
						confirmButtonText:"Cerrar"
					}).then(function(result){
						if(result.value){
							window.location = "index.php?pagina=registro";
						}
					});

					</script>';
				}
			}else{

				echo '<script>

				Swal.fire({
					
					icon: "error",
					title: "No se permiten caracteres especiales en el nombre",
					showConfirmButton: true,
					confirmButtonText:"Cerrar"
				}).then(function(result){
					if(result.value){
						window.location = "index.php?pagina=registro";
					}
				});

				</script>';
			}
		}
	}				

	
	/*=============================================
	Eliminar Registro
	=============================================*/
	public function ctrEliminarProducto(){

		if(isset($_GET["idEliminarProducto"])){

			$tabla = "producto";
			$valor = $_GET["idEliminarProducto"];

			$respuesta = ModeloProductos::mdlEliminarProducto($tabla, $valor);

			if($respuesta == "ok"){

				echo '<script>

				Swal.fire({
					
					icon: "success",
					title: "El producto se eliminó correctamente",
					showConfirmButton: true,
					confirmButtonText:"Cerrar"
				  }).then(function(result){
					if(result.value){
						window.location = "index.php?pagina=productos";
					}
				  });

				</script>';

			}

		}

	}
}