<?php

class ControladorVenta{

    /*=============================================
	Seleccionar Registros
	=============================================*/

	static public function ctrSeleccionarVentas($item, $valor){

		$tabla = "venta";

		$respuesta = ModeloVenta::mdlSeleccionarVentas($tabla, $item, $valor);

		return $respuesta;

	}

	
	/*=============================================
	Registro
	=============================================*/

	static public function ctrRegistroVenta(){

		if(isset($_POST["producto"])){

			$tabla = "producto";

			$datos = array("importeTotal" => $_POST["importeTotal"]);	 

			$respuesta = ModeloVenta::mdlRegistroVenta($tabla, $datos);

			if($respuesta == "ok"){

				echo '<script>

					if ( window.history.replaceState ) {

						window.history.replaceState( null, null, window.location.href );

					}

					window.location = "index.php?pagina=productos";

				</script>';

			}else{
				echo $respuesta;
			}
		}
	}

}

// /*=============================================
// 	Eliminar Registro
// 	=============================================*/
// 	public function ctrEliminarUsuario(){

// 		if(isset($_GET["idEliminarUsuario"])){

// 			$tabla = "usuario";
// 			$valor = $_GET["idEliminarUsuario"];

// 			$respuesta = ModeloUsuarios::mdlEliminarUsuarios($tabla, $valor);

// 			if($respuesta == "ok"){

// 				echo '<script>

// 				Swal.fire({
					
// 					icon: "success",
// 					title: "El usuario se eliminó correctamente",
// 					showConfirmButton: true,
// 					confirmButtonText:"Cerrar"
// 				  }).then(function(result){
// 					if(result.value){
// 						window.location = "index.php?pagina=usuarios";
// 					}
// 				  });

// 				</script>';

// 			}

// 		}

// 	}
// }