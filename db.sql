/*----------------Borrar esto antes de crear las tablas------------------*/

/*DROP DATABASE IF EXIST grupo3;*/

/*CREATE DATABASE grupo3;*/

/*-------------------------Producto-------------------------------*/

DROP TABLE IF EXISTS `producto`;

SET FOREIGN_KEY_CHECKS=0;

CREATE TABLE `producto` (
  `idProducto` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text DEFAULT NULL,
  `marca` text DEFAULT NULL,
  `detalle` text DEFAULT NULL,
  `precio` float DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  PRIMARY KEY (`idProducto`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

/*--------------------------Venta-------------------------------*/

/*DROP TABLE IF EXISTS `venta`;*/

SET FOREIGN_KEY_CHECKS=0;

CREATE TABLE `Venta` (
  `idVenta` int(11) NOT NULL AUTO_INCREMENT,
  `importeTotal` float DEFAULT NULL,
  `fechaVenta` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`idVenta`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

/*------------------------Detalle---------------------------------*/

/*DROP TABLE IF EXISTS `detalle`;*/

SET FOREIGN_KEY_CHECKS=0;

CREATE TABLE `detalle` (
  `idDetalle` int(0) NOT NULL AUTO_INCREMENT,
  `idVenta` int(0) NOT NULL,
  `idProducto`int(0) NOT NULL,
  `cantidad`int(0) NOT NULL,
  PRIMARY KEY (`idDetalle`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

ALTER TABLE `detalle`
ADD FOREIGN KEY(`idVenta`) REFERENCES `venta`(`idVenta`),
ADD FOREIGN KEY(`idProducto`) REFERENCES `producto`(`idProducto`);
