<?php

require_once './controladores/plantilla.controlador.php';
require_once './controladores/producto.controlador.php';
require_once './modelos/producto.modelo.php';
require_once './controladores/venta.controlador.php';
require_once './modelos/venta.modelo.php';
require_once './controladores/detallesVenta.controlador.php';
require_once './modelos/detallesVenta.modelo.php';

$plantilla = new ControladorPlantilla();
$plantilla -> ctrMostrarPlantilla();