$(".tablaUsuarios tbody").on("click", ".btnEliminarProducto", function(){

  var idProducto = $(this).attr("idProducto");

  Swal.fire({
      title: 'Quiere eliminar el producto?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'eliminar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        window.location = "index.php?pagina=productos&idEliminarProducto="+idProducto;
      }
    })
})

// //traer datos del usuario
// $(".tablaUsuarios").on("click", ".btnVerUsuario", function(){

// 	var idUsuario = $(this).attr("idUsuario");
	
// 	var datos = new FormData();
// 	datos.append("idUsuario", idUsuario);

// 	$.ajax({

// 		url:"ajax/usuarios.ajax.php",
// 		method: "POST",
// 		data: datos,
// 		cache: false,
// 		contentType: false,
// 		processData: false,
// 		dataType: "json",
// 		success: function(respuesta){

//       console.log(respuesta);
			
// 			$(".nombreUsuario").html(respuesta["nombre"]);
// 			$(".email").html(respuesta["email"]);			

// 		}

// 	});

// })