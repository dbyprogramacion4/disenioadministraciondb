<?php

$Ventas = ControladorDetallesVenta::ctrSeleccionarDetallesVenta(null, null);

?>
<div class="d-flex justify-content-center mb-4">
    <h1>Detalles de Venta</h1>
</div>
<table class="table table-striped tablaUsuarios">
    <thead>
        <tr>
            <th>Producto</th>
            <th>Marca</th>
            <th>Detalle</th>
            <th>Precio Unitario</th>
            <th>Cantidad</th>
            <th>Stock</th>				
        </tr>
    </thead>
    <tbody>

    <?php 
    foreach($Ventas as $value){
    ?>
        <tr>
            <td><?php echo $value["idVenta"]; ?></td>
            <td><?php echo $value["fechaVenta"]; ?></td>
            <td><a href="index.php?pagina=detalleVenta&id=<?php echo $value["idVenta"]; ?>" class="btn button">Detalles de venta</a></td>
            <td>
                <button class="btn btn-danger btnEliminarUsuario" idUsuario="<?php echo $value["idVenta"]; ?>"><i class="fa fa-trash"></i></button>
            </td> 
        </tr>

        <?php } ?>

    </tbody>
</table>
