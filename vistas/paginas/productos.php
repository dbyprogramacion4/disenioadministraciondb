<?php

$productos = ControladorProducto::ctrSeleccionarProductos(null, null);

?>
<div class="d-flex justify-content-center mb-4">
    <h1>Productos</h1>
</div>
<table class="table table-striped tablaUsuarios">
    <thead>
        <tr>
            <th>Producto</th>
            <th>Marca</th>
            <th>Detalle</th>
            <th>Precio Unitario</th>
            <th>estado</th>
            <th>Stock</th>
            <th>Eliminar</th>	
        </tr>
    </thead>
    <tbody>

    <?php 
    foreach($productos as $value){
    ?>
        <tr>
            <td><?php echo $value["nombre"]; ?></td>
            <td><?php echo $value["marca"]; ?></td>
            <td><?php echo $value["detalle"]; ?></td>
            <td><?php echo $value["precio"]; ?></td>
            <td><?php echo $value["estado"]; ?></td>
            <td><?php echo $value["stock"]; ?></td>
            <td>
                <button class="btn btn-danger btnEliminarProducto" idProducto="<?php echo $value["idProducto"]; ?>">Eliminar</button>
            </td> 
        </tr>

        <?php } ?>

    </tbody>
</table>

<?php

$eliminarProducto = new ControladorProducto();
$eliminarProducto -> ctrEliminarProducto();

?>
