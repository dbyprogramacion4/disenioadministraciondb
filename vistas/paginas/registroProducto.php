<form method="post" id="formRegistroProducto" enctype="multipart/form-data">

    <div class="form-group">
        <label for="email">Nombre:</label>
        <input type="text" class="form-control" name="nombre">
    </div>

    <div class="form-group">
        <label for="email">Marca:</label>
        <input type="text" class="form-control" name="marca">
    </div>

    <div class="form-group">
        <label for="email">Detalle:</label>
        <input type="text" class="form-control" name="detalle">
    </div>

    <div class="form-group">
        <label for="password">Precio:</label>
        <input type="text" class="form-control" name="precio">
    </div>

    <div class="form-group">
        <label for="password">Stock:</label>
        <input type="text" class="form-control" name="stock">
    </div>

    <?php

    $crearProducto = new ControladorProducto();
    $crearProducto -> ctrRegistroProducto();
    
    ?>
    <div class="d-flex justify-content-center">
        <button type="submit" class="btn button">Registrar</button>
    </div>
</form>