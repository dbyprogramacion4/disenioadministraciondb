<form method="post" id="formRegistro" enctype="multipart/form-data">


    <div class="form-group" >

        <select name="idProducto" class="form-control">

            <option value="">Seleccionar</option>

            <?php $productos = ControladorProducto::ctrSeleccionarProductos(null,null); 

            foreach ($productos as $key => $value) { ?>

                <option value="<?php echo $value["idProducto"]; ?>"><?php echo $value["nombre"]; ?></option>
            
            <?php } ?>
            
        </select>

    </div>
                
    <div class="form-group">
        <label for="email">Cantidad:</label>
        <input type="text" class="form-control" name="cantidad">
    </div>

    <?php

    $crearVenta = new ControladorVenta();
    $crearVenta -> ctrRegistroVenta();

    
    $detalleVenta = new ControladorDetallesVenta();		   
	$detalleVenta -> ctrRegistroDetallesVenta();	
    
    ?>
    
    <div class="d-flex justify-content-center">
        <button type="submit" class="btn button">Guardar</button>
    </div>
    
</form>