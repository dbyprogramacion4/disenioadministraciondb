<?php

$Ventas = ControladorVenta::ctrSeleccionarVentas(null, null);

?>
<div class="d-flex justify-content-center mb-4">
    <h1>Ventas</h1>
</div>
<table class="table table-striped tablaUsuarios">
    <thead>
        <tr>
            <th>Numero</th>
            <th>fecha</th>
            <th>Detalle de venta</th>					
        </tr>
    </thead>
    <tbody>

    <?php 
    foreach($Ventas as $value){
    ?>
        <tr>
            <td><?php echo $value["idVenta"]; ?></td>
            <td><?php echo $value["fechaVenta"]; ?></td>
            <td><a href="index.php?pagina=detallesVenta&id=<?php echo $value["idVenta"]; ?>" class="btn button">Detalles de venta</a></td>
        </tr>

        <?php } ?>

    </tbody>
</table>
