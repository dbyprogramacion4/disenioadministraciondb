

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="./vistas/css/styles.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
        <script></script>
        <title>Document</title>
        <!-- Latest sweetalert2-->
	<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

        <!-- Latest compiled and minified CSS -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <!-- Popper JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    </head>
    <body>
        <nav class="navbar navbar-expand-lg nav__background">
            <div class=" d-flex w-100">
                <a class="navbar-brand" href="#">
                    <img src="" class="shadow rounded-3 me-2 logo" alt="">
                    Maxi kiosco P.E.M
                </a>
                <div class="d-flex justify-content-center w-100">
                    <div class="dropdown me-5">
                        <button class="btn button dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Productos
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <li><a class="dropdown-item" href="index.php?pagina=productos">Productos</a></li>
                            <li><a class="dropdown-item" href="index.php?pagina=registroProducto">Nuevo producto</a></li>
                        </div>
                    </div>
                    <div class="dropdown me-5 pe-5">
                        <button class="btn button dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Ventas
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <li><a class="dropdown-item" href="index.php?pagina=ventas">Ventas</a></li>
                            <li><a class="dropdown-item" href="index.php?pagina=registroVenta">Nueva venta</a></li>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <main class="main__flex d-flex my-5">
            <div class="container-fluid">
                <div class="container py-5">
                        <?php if(isset($_GET["pagina"])){
                            if($_GET["pagina"] == "productos" ||
                                $_GET["pagina"] == "registroProducto"||
                                $_GET["pagina"] == "ventas"||
                                $_GET["pagina"] == "registroVenta"||
                                $_GET["pagina"] == "detallesVenta"
                            ){
                                include "paginas/".$_GET["pagina"].".php";
                            }
                        }else{
                            include "paginas/productos.php";
                        }
                    ?>
                </div>
            </div>
        </main>
        <footer class="d-flex justify-content-end main__footer footer__index">
            <div class="text-end me-5 p-4">
            Creado por:  Patricio, Ezequiel y Matias
            </div>
        </footer>
    </body>
    <script src="./vistas/js/producto.js"></script>
</html>